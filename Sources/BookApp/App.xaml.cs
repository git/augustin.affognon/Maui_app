﻿using ViewModel;

namespace BookApp;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();
         MainPage = new AppShell();
        Routing.RegisterRoute("player", typeof(Player));

    }
}

