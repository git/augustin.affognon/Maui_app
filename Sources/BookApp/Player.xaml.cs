﻿using System.Collections.ObjectModel;

namespace BookApp;

public partial class Player : ContentPage
{
    public List<string> MyItems { get; set; }
    public Player()
	{
		InitializeComponent();
        MyItems = new List<string>();

        // Ajouter des éléments à la liste
        MyItems.Add("Élément 1");
        MyItems.Add("Élément 2");
        MyItems.Add("Élément 3");
        MyItems.Add("Élément 4");

        myListView.ItemsSource = MyItems; 
     }
    private async void OnBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }


    
}
