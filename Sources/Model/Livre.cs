﻿using System.Numerics;

namespace Model;
public class Livre : IEquatable<Livre>
{
    private string title;
    public string Title
    {
        get => title;
        private set
        {
            title = string.IsNullOrWhiteSpace(value) ? "" : value;
        }
    }


    private string auteur;
    public string Auteur
    {
        get => auteur;
        private set
        {
            auteur = string.IsNullOrWhiteSpace(value) ? "" : value;
        }
    }


    public Livre(string nom, string auteur)
    {
        this.Title = nom;
        this.Auteur = auteur;
        if (string.IsNullOrWhiteSpace(Title)
    && string.IsNullOrWhiteSpace(Auteur) )
        {
            throw new ArgumentException("un morceaui doit avoir un titre et un auteur ");
        }

    }

     public void setTitle(string title)
    {
        Title = title;
    }

    public void setAuteur(string auteur)
    {
        Auteur = auteur;
    }

    public bool Equals(Livre? obj)
    {
        if (ReferenceEquals(obj, null)) return false;
        if (ReferenceEquals(obj, this)) return true;
        if (GetType() != obj.GetType()) return false;
        return Equals(obj as Livre);

    }
}

