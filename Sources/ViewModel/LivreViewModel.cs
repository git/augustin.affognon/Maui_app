﻿using Model;

namespace ViewModel;
public class LivreViewModel
{
    public Livre model { set; get; }

     
    public LivreViewModel(Livre livre)
    {
        model = livre;
    }

    public void setTitle(string title)
    {
        model.setTitle ( title);
    }

    public void SetAuteur(string auteur)
    {
        model.setAuteur(auteur);
    }
}

