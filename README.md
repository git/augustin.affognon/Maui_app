
[![Build Status](https://codefirst.iut.uca.fr/api/badges/augustin.affognon/Maui_app/status.svg?ref=refs/heads/squ_614252d3bc92df28e0ccaf493ea8a00f98f5ac14)](https://codefirst.iut.uca.fr/augustin.affognon/Maui_app)
[![Quality Gate Status](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=alert_status&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
 [![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=bugs&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=code_smells&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Coverage](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=coverage&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Vulnerabilities](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=vulnerabilities&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Duplicated Lines (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=duplicated_lines_density&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Reliability Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=reliability_rating&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
[![Security Hotspots](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=security_hotspots&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
 [![Security Rating](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=security_rating&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
 [![Technical Debt](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=Maui_app&metric=sqale_index&token=cd56bcaa28011a3f7aa90073ceef54a42e455147)](https://codefirst.iut.uca.fr/sonar/dashboard?id=Maui_app)
 
# Maui_app

Welcome on the Maui_app project!  

  

_Generated with a_ **Code#0** _template_  
<img src="Documentation/doc_images/CodeFirst.png" height=40/>   